/*
 * $Id: Scribble.c,v 1.1.1.1 2000/06/12 22:20:12 jg Exp $
 *
 * Copyright � 1999 Keith Packard
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Keith Packard not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Keith Packard makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * KEITH PACKARD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL KEITH PACKARD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/keysym.h>
#include "ScribbleP.h"
#include <hre_api.h>
#include <li_recognizer.h>

static XtResource resources[] = {
#define offset(field) XtOffsetOf(ScribbleRec, scribble.field)
    { XtNforeground, XtCForeground, XtRPixel, sizeof (unsigned long),
	offset (foreground), XtRString, XtDefaultForeground },
    { XtNmodeCallback, XtCCallback, XtRCallback, sizeof (XtPointer),
	offset (modeCallbacks), XtRCallback, (XtPointer) NULL},
    { XtNkeysymCallback, XtCCallback, XtRCallback, sizeof (XtPointer),
	offset (keysymCallbacks), XtRCallback, (XtPointer) NULL},
    { XtNthickness, XtCThickness, XtRDimension, sizeof (Dimension),
	offset (thickness), XtRImmediate, (XtPointer) 3 },
#undef offset
};

static void	Initialize(), Destroy (), Redisplay ();
static Boolean	SetValues ();

static char defaultTranslations[] = {
    "<BtnDown>: strokeStart()\n"
    "<BtnMotion>: strokeMove()\n"
    "<BtnUp>: strokeEnd()"
};

static void ActionStart ();
static void ActionMove ();
static void ActionEnd ();

static int
graffiti_load_recognizers(struct graffiti *pg);
    
static void
Recognize (ScribbleWidget w);
    
static XtActionsRec actions[] = {
    { "strokeStart", ActionStart },
    { "strokeMove", ActionMove },
    { "strokeEnd", ActionEnd }
};

ScribbleClassRec scribbleClassRec = {
  { /* core fields */
    /* superclass		*/	(WidgetClass) &widgetClassRec,
    /* class_name		*/	"Scribble",
    /* widget_size		*/	sizeof(ScribbleRec),
    /* class_initialize		*/	NULL,
    /* class_part_initialize	*/	NULL,
    /* class_inited		*/	FALSE,
    /* initialize		*/	Initialize,
    /* initialize_hook		*/	NULL,
    /* realize			*/	XtInheritRealize,
    /* actions			*/	actions,
    /* num_actions		*/	XtNumber(actions),
    /* resources		*/	resources,
    /* num_resources		*/	XtNumber(resources),
    /* xrm_class		*/	NULLQUARK,
    /* compress_motion		*/	FALSE,
    /* compress_exposure	*/	TRUE,
    /* compress_enterleave	*/	TRUE,
    /* visible_interest		*/	FALSE,
    /* destroy			*/	Destroy,
    /* resize			*/	NULL,
    /* expose			*/	Redisplay,
    /* set_values		*/	SetValues,
    /* set_values_hook		*/	NULL,
    /* set_values_almost	*/	XtInheritSetValuesAlmost,
    /* get_values_hook		*/	NULL,
    /* accept_focus		*/	NULL,
    /* version			*/	XtVersion,
    /* callback_private		*/	NULL,
    /* tm_table			*/	defaultTranslations,
    /* query_geometry		*/	XtInheritQueryGeometry,
    /* display_accelerator	*/	XtInheritDisplayAccelerator,
    /* extension		*/	NULL
  },
  { /* simple fields */
    /* empty			*/	0
  },
  { /* scribble fields */
    /* empty			*/	0
  }
};

WidgetClass scribbleWidgetClass = (WidgetClass)&scribbleClassRec;

static void
ResetStroke (ScribbleWidget w)
{
    w->scribble.ps.ps_npts = 0;
    w->scribble.ps.ps_nstate = 0;
    w->scribble.ps.ps_trans = 0;
    w->scribble.ps.ps_state = 0;
    if (XtIsRealized ((Widget) w))
	XClearArea (XtDisplay ((Widget) w), 
		    XtWindow ((Widget) w), 0, 0, 0, 0, False);
}

static void
DisplayStroke (ScribbleWidget	w)
{
    XDrawLines (XtDisplay (w), XtWindow (w), w->scribble.foregroundGC,
		w->scribble.pt,
		w->scribble.ps.ps_npts,
		CoordModeOrigin);
}

static void
DisplayLast (ScribbleWidget w)
{
    int	    npt;
    
    npt = w->scribble.ps.ps_npts;
    if (npt > 2)
	npt = 2;
    XDrawLines (XtDisplay (w), XtWindow (w), w->scribble.foregroundGC,
		w->scribble.pt + (w->scribble.ps.ps_npts - npt),
		npt,
		CoordModeOrigin);
}

static void
AddPoint (ScribbleWidget    w,
	  int		    x,
	  int		    y)
{
    pen_point	*ppa;
    XPoint	*pt;
    int		ppasize;
    
    if (w->scribble.ps.ps_npts == w->scribble.ppasize)
    {
	ppasize = w->scribble.ppasize + 100;
	ppa = malloc ((sizeof (pen_point) + sizeof (XPoint)) * ppasize);
	if (!ppa)
	    return;
	pt = (XPoint *) (ppa + ppasize);
	memcpy (ppa, w->scribble.ps.ps_pts, 
		w->scribble.ppasize * sizeof (pen_point));
	memcpy (pt, w->scribble.pt,
		w->scribble.ppasize * sizeof (XPoint));
	free (w->scribble.ps.ps_pts);
	w->scribble.ps.ps_pts = ppa;
	w->scribble.pt = pt;
	w->scribble.ppasize = ppasize;
    }
    ppa = &w->scribble.ps.ps_pts[w->scribble.ps.ps_npts];
    ppa->x = x;
    ppa->y = y;
    
    pt = &w->scribble.pt[w->scribble.ps.ps_npts];
    pt->x = x;
    pt->y = y;
    
    w->scribble.ps.ps_npts++;
    
    DisplayLast (w);
}

static void
GetGC (ScribbleWidget	w)
{
    XGCValues		gcv;
    
    gcv.foreground = w->scribble.foreground;
    gcv.line_width = w->scribble.thickness;
    gcv.join_style = JoinRound;
    gcv.cap_style = CapRound;
    w->scribble.foregroundGC = XtGetGC ((Widget) w, 
					GCForeground|
					GCLineWidth|
					GCJoinStyle|
					GCCapStyle,
					&gcv);
}

static void
Initialize (greq, gw)
    Widget  greq, gw;
{
    ScribbleWidget	req = (ScribbleWidget) greq,
			new = (ScribbleWidget) gw;

    if (new->core.width < 1) new->core.width = 100;
    if (new->core.height < 1) new->core.height = 100;
    
    new->scribble.capsLock = 0;
    new->scribble.puncShift = 0;
    new->scribble.tmpShift = 0;
    new->scribble.ctrlShift = 0;
    new->scribble.curCharSet = CS_LETTERS;
    
    graffiti_load_recognizers (&new->scribble.graf);
    
    GetGC (new);
    
    new->scribble.ppasize = 0;
    new->scribble.ps.ps_pts = 0;
    new->scribble.pt = 0;
    
    ResetStroke (new);
}

static void
Destroy (gw)
    Widget  gw;
{
    ScribbleWidget  w = (ScribbleWidget) gw;

    XtReleaseGC (gw, w->scribble.foregroundGC);
}

static void
Redisplay (gw, event, region)
    Widget  gw;
    XEvent  *event;
    Region  region;
{
    ScribbleWidget  w = (ScribbleWidget) gw;
    int		    i;

    DisplayStroke (w);
}

static Boolean
SetValues (gcur, greq, gw)
    Widget  gcur, greq, gw;
{
    ScribbleWidget  cur = (ScribbleWidget) gcur,
		    req = (ScribbleWidget) greq,
		    new = (ScribbleWidget) gw;
    XGCValues	    gcv;
    Boolean	    redraw = FALSE;

    if (req->scribble.foreground != cur->scribble.foreground ||
	req->scribble.thickness != cur->scribble.thickness)
    {
	XtReleaseGC (gcur, cur->scribble.foregroundGC);
	GetGC (new);
	redraw = TRUE;
    }
    return redraw;
}

static void
ActionStart (Widget gw, XEvent *event, String *params, Cardinal *num_params)
{
    ScribbleWidget  w = (ScribbleWidget) gw;
    
    ResetStroke (w);
    AddPoint (w, event->xbutton.x, event->xbutton.y);
}

static void
ActionMove (Widget gw, XEvent *event, String *params, Cardinal *num_params)
{
    ScribbleWidget  w = (ScribbleWidget) gw;
    
    AddPoint (w, event->xmotion.x, event->xmotion.y);
}

static void
ActionEnd (Widget gw, XEvent *event, String *params, Cardinal *num_params)
{
    ScribbleWidget  w = (ScribbleWidget) gw;
    
    AddPoint (w, event->xbutton.x, event->xbutton.y);
    Recognize (w);
}


/* This procedure is called to initialize pg by loading the three
   recognizers, loading the initial set of three classifiers, and
   loading & verifying the recognizer extension functions.  If the
   directory $HOME/.recognizers exists, the classifier files will be
   loaded from that directory.  If not, or if there is an error, the
   default files (directory specified in Makefile) will be loaded
   instead.  Returns non-zero on success, 0 on failure.  (Adapted from
   package tkgraf/src/GraffitiPkg.c. */

static int
graffiti_load_recognizers(struct graffiti *pg)
{
	bool usingDefault;
	char* homedir;
	int i;
	rec_fn *fns;

	/* First, load the recognizers... */
	/* call recognizer_unload if an error ? */
	for (i = 0; i < NUM_RECS; i++) {
		int rec_return;
		/* Load the recognizer itself... */
		pg->rec[i] = recognizer_load(DEFAULT_REC_DIR, rec_name, NULL);
		if (pg->rec[i] == NULL) {
			fprintf(stderr,"Error loading recognizer from %s.", DEFAULT_REC_DIR);
			return 0;
		}
		if ((* (int *)(pg->rec[i])) != 0xfeed) {
			fprintf(stderr,"Error in recognizer_magic.");
			return 0;
		}
	}

	/* ...then figure out where the classifiers are... */
	if ( (homedir = (char*)getenv("HOME")) == NULL ) {
		strcpy(pg->cldir, REC_DEFAULT_USER_DIR);
		usingDefault = true;
	} else {
		strcpy(pg->cldir, homedir);
		strcat(pg->cldir, "/"); 
		strcat(pg->cldir, CLASSIFIER_DIR); 
		usingDefault = false;
	}

	/* ...then load the classifiers... */
	for (i = 0; i < NUM_RECS; i++) {
		int rec_return;
		char *s;

		rec_return = recognizer_load_state(pg->rec[i], pg->cldir, cl_name[i]);
		if ((rec_return == -1) && (usingDefault == false)) {
			fprintf(stderr,
				"Unable to load custom classifier file %s/%s.\nTrying default classifier file instead.\nOriginal error: %s\n ", 
				pg->cldir, cl_name[i], 
				(s = recognizer_error(pg->rec[i])) ? s : "(none)");
			rec_return = recognizer_load_state(pg->rec[i],
							   REC_DEFAULT_USER_DIR, cl_name[i]);
		}
		if (rec_return == -1) {
			fprintf(stderr, "Unable to load default classifier file %s.\nOriginal error: %s\n",
				cl_name[i], 
				(s = recognizer_error(pg->rec[i])) ? s : "(none)");
			return 0;
		}
	}

	/* We have recognizers and classifiers now.   */
	/* Get the vector of LIextension functions..     */
	fns = recognizer_get_extension_functions(pg->rec[CS_LETTERS]);
	if (fns == NULL) {
		fprintf(stderr, "LI Recognizer Training:No extension functions!");
		return 0;
	}
	
	/* ... and make sure the training & get-classes functions are okay. */
	if( (pg->rec_train = (li_recognizer_train)fns[LI_TRAIN]) == NULL ) {
		fprintf(stderr,
			"LI Recognizer Training:li_recognizer_train() not found!");
		if (fns != NULL) {
			free(fns);
		}
		return 0;
	}
  
	if( (pg->rec_getClasses = (li_recognizer_getClasses)fns[LI_GET_CLASSES]) == NULL ) {
		fprintf(stderr,
			"LI Recognizer Training:li_recognizer_getClasses() not found!");
		if (fns != NULL) {
			free(fns);
		}
		return 0;
	}
	free(fns);
	return 1;
}

static void
Notify (ScribbleWidget	w, KeySym keysym)
{
}

static void
ShowMode (ScribbleWidget w)
{
    ScribbleModeDataRec	md;

    md.controlShift = w->scribble.ctrlShift;
    md.puncShift = w->scribble.puncShift;
    md.capsLock = w->scribble.capsLock;
    md.shift = w->scribble.tmpShift;
    md.num = w->scribble.curCharSet == CS_DIGITS;
    XtCallCallbackList ((Widget) w, w->scribble.modeCallbacks, &md);
}

static void
SendKeysym (ScribbleWidget w, KeySym keysym, Boolean control)
{
    ScribbleKeysymDataRec   kd;

    kd.control = control;
    kd.keysym = keysym;
    XtCallCallbackList ((Widget) w, w->scribble.keysymCallbacks, &kd);
}

static char
do_recognize(struct graffiti *pg, pen_stroke *ps, int charset)
{
       char rec_char;
       int i, nret;
       rec_alternative *ret;

       rec_char = recognizer_translate(pg->rec[charset], 1, ps, false,
				       &nret, &ret);
       if (rec_char != -1) {
	       delete_rec_alternative_array(nret, ret, false);
       }
       return rec_char;
}

static void
Recognize (ScribbleWidget w)
{
    struct graffiti *graf = &w->scribble.graf;
    pen_stroke	    *ps = &w->scribble.ps;
    KeySym	    keysym;
    Boolean	    control;
    char	    c;
    
    if (ps->ps_npts == 0)
	return;
    
    c = do_recognize(graf, ps, 
		     w->scribble.puncShift ? CS_PUNCTUATION : 
		     w->scribble.curCharSet);
    switch (c) {
    case '\000':
	w->scribble.tmpShift = 0;
	w->scribble.puncShift = 0;
	w->scribble.ctrlShift = 0;
	ShowMode (w);
	break;
    case 'L': /* caps lock */
	w->scribble.capsLock = !w->scribble.capsLock;
	ShowMode (w);
	break;
    case 'N': /* numlock */
	if (w->scribble.curCharSet == CS_DIGITS) {
	    w->scribble.curCharSet = CS_LETTERS;
	} else {
	    w->scribble.curCharSet = CS_DIGITS;
	}
	w->scribble.tmpShift = 0;
	w->scribble.puncShift = 0;
	w->scribble.ctrlShift = 0;
	ShowMode (w);
	break;
    case 'P': /* usually puncshift, but we'll make it CTRL */
	w->scribble.ctrlShift = !w->scribble.ctrlShift;
	w->scribble.tmpShift = 0;
	w->scribble.puncShift = 0;
	ShowMode (w);
	break;
    case 'S': /* shift */
	w->scribble.tmpShift = !w->scribble.tmpShift;
	w->scribble.puncShift = 0;
	w->scribble.ctrlShift = 0;
	ShowMode (w);
	break;
    default:
	control = FALSE;
	switch (c) {
	case 'A':
	    keysym = XK_space;
	    break;
	case 'B':
	    keysym = XK_BackSpace;
	    break;
	case 'R':
	    keysym = XK_Return;
	    break;
	case '.':
	    if (! w->scribble.puncShift) {
		w->scribble.puncShift = 1;
		w->scribble.ctrlShift = 0;
		w->scribble.tmpShift = 0;
		ShowMode (w);
		return;
	    } else {
		w->scribble.puncShift = 0;
		ShowMode (w);
	    }		  	
	    keysym = XK_period;
	    break;
	default:
	    if ('A' <= c && c <= 'Z')
		return;
	    keysym = (KeySym) c;
	    if (w->scribble.ctrlShift) 
	    {
		control = TRUE;
		w->scribble.ctrlShift = 0;
		if (c < 'a' || 'z' < c)
		{
		    ShowMode (w);
		    return;
		}
	    } 
	    else if ((w->scribble.capsLock && !w->scribble.tmpShift) || 
		     (!w->scribble.capsLock && w->scribble.tmpShift)) 
	    {
		KeySym	lower, upper;

		XConvertCase (keysym, &lower, &upper);
		keysym = upper;
	    } 
	    w->scribble.tmpShift = 0;
	    w->scribble.puncShift = 0;
	    ShowMode(w);
	}
	SendKeysym (w, keysym, control);
	break;
    }
}
