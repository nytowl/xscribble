/*
 * $Id: ScribbleP.h,v 1.1.1.1 2000/06/12 22:20:12 jg Exp $
 *
 * Copyright � 1999 Keith Packard
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Keith Packard not be used in
 * advertising or publicity pertaining to distribution of the software without
 * specific, written prior permission.  Keith Packard makes no
 * representations about the suitability of this software for any purpose.  It
 * is provided "as is" without express or implied warranty.
 *
 * KEITH PACKARD DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL KEITH PACKARD BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _ScribbleP_h
#define _ScribbleP_h

#include "Scribble.h"
/* include superclass private header file */
#include <X11/Xaw/SimpleP.h>
#include <hre_api.h>
#include <li_recognizer.h>

#define CS_LETTERS     0
#define CS_DIGITS      1
#define CS_PUNCTUATION 2

#define NUM_RECS    3
#define DEFAULT_REC_DIR         "classsifiers"
#define REC_DEFAULT_USER_DIR    "/mnt/ram2/scribble/classifiers"
#define CLASSIFIER_DIR          ".classifiers"
#define DEFAULT_LETTERS_FILE    "letters.cl"
#define DEFAULT_DIGITS_FILE     "digits.cl"
#define DEFAULT_PUNC_FILE       "punc.cl"
#define rec_name                "libli_recog.so"

static char *cl_name[3] = {DEFAULT_LETTERS_FILE, 
			     DEFAULT_DIGITS_FILE,
			     DEFAULT_PUNC_FILE};

struct graffiti {
	recognizer rec[3];     /* 3 recognizers, one each for letters, digits, 
				  and punctuation */
	char cldir[200];       /* directory in which the current classifier
				  files are found */
	li_recognizer_train rec_train; /* pointer to training function */
	li_recognizer_getClasses rec_getClasses; 
    	                       /* pointer to the function that lists
                           	  the characters in the classifier
                           	  file. */
};

/* define unique representation types not found in <X11/StringDefs.h> */

#define XtRScribbleResource "ScribbleResource"

typedef struct {
    int empty;
} ScribbleClassPart;

typedef struct _ScribbleClassRec {
    CoreClassPart	core_class;
    SimpleClassPart	simple_class;
    ScribbleClassPart	scribble_class;
} ScribbleClassRec;

extern ScribbleClassRec scribbleClassRec;

typedef struct {
    /* resources */
    unsigned long   foreground;
    XtCallbackList  modeCallbacks;
    XtCallbackList  keysymCallbacks;
    Dimension	    thickness;
    
    /* private state */
    GC		    foregroundGC;
    XPoint	    *pt;	    /* X points */
    int		    ppasize;
    pen_stroke	    ps;
    struct graffiti graf;
    int		    capsLock;
    int		    puncShift;
    int		    tmpShift;
    int		    ctrlShift;
    int		    curCharSet;
} ScribblePart;

typedef struct _ScribbleRec {
    CorePart		core;
    SimplePart		simple;
    ScribblePart	scribble;
} ScribbleRec;

typedef ScribbleRec *ScribbleWidget;

#endif /* _ScribbleP_h */
